#!/bin/bash

# Atualiza o cache de pacotes
apt-get update

# Instala as dependências
apt-get install -y build-essential libssl-dev curl

# Instala o Node.js (como exemplo de uma dependência)
curl -sL https://deb.nodesource.com/setup_lts.x | -E bash -
apt-get install -y nodejs
